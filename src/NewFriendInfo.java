import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/27.
 */
public class NewFriendInfo implements Serializable {
    private String username;
    public NewFriendInfo(String name){
        username = name;
    }
    @Override
    public String toString(){
        return "username is " + username;
    }
    public String getUsername(){
        return username;
    }
}
