/**
 * Created by Administrator1 on 2016/11/13.
 */
import java.io.Serializable;
public class LoadfriendlistInfoRes implements Serializable{
    private int[] userlist;
    private String[] username;
    private int[] useronline;
    public LoadfriendlistInfoRes(int[] list,String[] name, int[] online ){
        userlist = list;
        username = name;
        useronline = online;
    }
    @Override
    public String toString(){
        return "user number is " + userlist.length;
    }
    public int[] getUserlist(){
        return userlist;
    }
    public int[] getUseronline(){
        return useronline;
    }
    public String[] getUsername(){
        return username;
    }
}
