import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator1 on 2016/10/30.
 */
public class Main{
    private static final int perioddate = 300;
    public static void main(String args[]){
        DBAccess.getDBA();
        try{
            ServerSocket serversocket = new ServerSocket(2333);
            final int userScale = 1000;
            ObjectOutputStream ooslist[] = new ObjectOutputStream[userScale];
            ObjectInputStream oislist[] = new ObjectInputStream[userScale];
            while(true){
                final Socket clientsocket = serversocket.accept();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int userid = 0;
                        try {
                            OutputStream os = clientsocket.getOutputStream();
                            InputStream is = clientsocket.getInputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(os);
                            ObjectInputStream ois = new ObjectInputStream(is);
                            DataOutputStream dos = new DataOutputStream(os);
                            DataInputStream dis = new DataInputStream(is);
                            RequestObject reqObj = null;
                            ResponseObject resObj = null;
                            while(true) {
                                reqObj = (RequestObject) ois.readObject();
                                System.out.println(reqObj);
                                switch (reqObj.getReqType()){
                                    case RequestObject.REQ_REG:
                                        RegInfo regInfo = (RegInfo)reqObj.getReqBody();
                                        ResultSet rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users WHERE BINARY uname='" +
                                                regInfo.getUsername()+"';");
                                        rs.next();
                                        int count = rs.getInt("COUNT(*)");
                                        if(count == 0) {
                                            rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users");
                                            rs.next();
                                            int id = rs.getInt("COUNT(*)");
                                            DBAccess.getDBA().execute("INSERT INTO users(uid,uname,password,online) " +
                                                    "VALUES(" + id + ",'" + regInfo.getUsername() + "','" + regInfo.getPassword() + "'," + 0 + ");");
                                            DBAccess.getDBA().executeUpdate("CREATE TABLE contact_"+id+"(" +
                                                    "uid INT(11) NOT NULL PRIMARY KEY," +
                                                    "uname VARCHAR(20) BINARY NOT NULL);");
                                            DBAccess.getDBA().executeUpdate("CREATE TABLE joinedgroup_"+id+"(" +
                                                    "groupid INT(11) NOT NULL PRIMARY KEY," +
                                                    "groupname VARCHAR(20) BINARY NOT NULL);");
//                                            DBAccess.getDBA().execute("INSERT INTO contact_"+id+"(uid,uname,online) " +
//                                                    "VALUES(" + 21 + ",'" + "luwenjie" + "',"  + 0 + ");");
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("successfully register"));
                                        }else{
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("fail to register"));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOG:
                                        LogInfo logInfo = (LogInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users WHERE BINARY password='" +
                                                logInfo.getPassword() + "' AND BINARY uname='" +
                                                logInfo.getUsername() + "';");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        if(count == 1){
                                            rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY password='" +
                                                    logInfo.getPassword() + "' AND BINARY uname='" +
                                                    logInfo.getUsername() + "';");
                                            rs.next();
                                            userid = rs.getInt("uid");
                                            DBAccess.getDBA().execute("UPDATE users SET online=1 WHERE uid="
                                                    +userid+";");
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("successfully login",userid));
                                            ooslist[userid] = oos;
                                            oislist[userid] = ois;
                                        }else{
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("fail to login",userid));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOADFRIENDLIST:
                                        LoadfriendlistInfo loadfriendlistInfo = (LoadfriendlistInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM contact_"+ loadfriendlistInfo.getID() +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        int[] userlist = new  int[count];
                                        String[] username = new String[count];
                                        int[] useronline = new int[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT users.uid,users.uname,online FROM users,contact_"+ loadfriendlistInfo.getID() +
                                                " WHERE users.uid=contact_"+ loadfriendlistInfo.getID()+".uid;");
                                        int i = 0;
                                        while(rs.next()){
                                            userlist[i] = rs.getInt("uid");
                                            username[i] = rs.getString("uname");
                                            useronline[i++] = rs.getInt("online");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADFRIENDLIST,new LoadfriendlistInfoRes(userlist,username,useronline));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_ADDNEWFRIEND:
                                        NewFriendInfo newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users WHERE BINARY uname='"
                                                + newFriendInfo.getUsername() + "';");
                                        rs.next();
                                        if(findSocketID(ooslist,oos) >= 0 && rs.getInt("COUNT(*)") != 0){
                                            rs = DBAccess.getDBA().executeQuery("SELECT uid,online FROM users WHERE BINARY uname='"
                                                    + newFriendInfo.getUsername() + "';");
                                            rs.next();
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid="+
                                                    findSocketID(ooslist,oos) + ";");
                                            rs2.next();
                                            if(rs.getInt("online") == 1) {
                                                resObj = new ResponseObject(ResponseObject.RES_ADDNEWFRIEND, new NewFriendInfoRes(rs2.getString("uname")));
                                                ooslist[rs.getInt("uid")].writeObject(resObj);
                                            }
                                        }
                                        break;
                                    case RequestObject.REQ_CONFIRMADDNEWFRIEND:
                                        newFriendInfo = (NewFriendInfo)reqObj.getReqBody();
                                        if(findSocketID(ooslist,oos) >= 0){
                                            rs = DBAccess.getDBA().executeQuery("SELECT uid,online FROM users WHERE BINARY uname='"
                                                    + newFriendInfo.getUsername() + "';");
                                            rs.next();
                                            ResultSet rs2 =  DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid="+
                                                    findSocketID(ooslist,oos) + ";");
                                            rs2.next();
                                            DBAccess.getDBA().execute("INSERT INTO contact_"+findSocketID(ooslist,oos)+"(uid,uname) " +
                                                    "VALUES(" + rs.getInt("uid") + ",'" + newFriendInfo.getUsername() + "');");
                                            DBAccess.getDBA().execute("INSERT INTO contact_"+rs.getInt("uid")+"(uid,uname) " +
                                                    "VALUES(" + findSocketID(ooslist,oos) + ",'" + rs2.getString("uname") + "');");
                                            if(rs.getInt("online") == 1){
                                                int id = rs.getInt("uid");
                                                rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM contact_"+ id +
                                                        " ;");
                                                rs.next();
                                                count = rs.getInt("COUNT(*)");
                                                userlist = new  int[count];
                                                username = new String[count];
                                                useronline = new int[count];
                                                rs = DBAccess.getDBA().executeQuery("SELECT users.uid,users.uname,online FROM users,contact_"+ id +
                                                        " WHERE users.uid=contact_"+ id+".uid;");
                                                i = 0;
                                                while(rs.next()){
                                                    userlist[i] = rs.getInt("uid");
                                                    username[i] = rs.getString("uname");
                                                    useronline[i++] = rs.getInt("online");
                                                }
                                                resObj = new ResponseObject(ResponseObject.RES_LOADFRIENDLIST,new LoadfriendlistInfoRes(userlist,username,useronline));
                                                ooslist[id].writeObject(resObj);
                                            }
                                            int id = findSocketID(ooslist,oos);
                                            rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM contact_"+ id +
                                                    " ;");
                                            rs.next();
                                            count = rs.getInt("COUNT(*)");
                                            userlist = new  int[count];
                                            username = new String[count];
                                            useronline = new int[count];
                                            rs = DBAccess.getDBA().executeQuery("SELECT users.uid,users.uname,online FROM users,contact_"+ id +
                                                    " WHERE users.uid=contact_"+ id+".uid;");
                                            i = 0;
                                            while(rs.next()){
                                                userlist[i] = rs.getInt("uid");
                                                username[i] = rs.getString("uname");
                                                useronline[i++] = rs.getInt("online");
                                            }
                                            resObj = new ResponseObject(ResponseObject.RES_LOADFRIENDLIST,new LoadfriendlistInfoRes(userlist,username,useronline));
                                            ooslist[id].writeObject(resObj);
                                        }
                                        break;
                                    case RequestObject.REQ_SENDMESSAGE:
                                        MessageInfo messageInfo = (MessageInfo)reqObj.getReqBody();
                                        Date date = new Date();
                                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                        String time = format.format(date);
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM chatstore;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        if(messageInfo.getMessagetype()!= MessageInfo.MESSAGETYPE_FILERECEIVE) {
                                            DBAccess.getDBA().execute("INSERT INTO chatstore(chatid,senderid,receiverid,messagetype,sendtimestamp) " +
                                                    "VALUES(" + count + "," + findSocketID(ooslist, oos) + "," + messageInfo.getReceiverid() + "," + messageInfo.getMessagetype() +
                                                    ",'" + time + "');");
                                        }
                                        switch (messageInfo.getMessagetype()){
                                            case MessageInfo.MESSAGETYPE_TEXTSENDFROMME:
                                                DBAccess.getDBA().execute("INSERT INTO textstore(chatid,content) " +
                                                        "VALUES(" +count+",'"+messageInfo.getChatmessage()+"');");
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_SENDMESSAGE, new MessageInfo(messageInfo.getReceiverid(),messageInfo.getChatmessage(),messageInfo.getMessagetype())));
                                                rs = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid="
                                                        + messageInfo.getReceiverid() + ";");
                                                rs.next();
                                                if(rs.getInt("online") == 1){
                                                    ooslist[messageInfo.getReceiverid()].writeObject(new ResponseObject(ResponseObject.RES_SENDMESSAGE, new MessageInfo(findSocketID(ooslist,oos),messageInfo.getChatmessage(),messageInfo.getMessagetype()+1)));
                                                }
                                                break;
                                            case MessageInfo.MESSAGETYPE_FILESENDFROMME: case MessageInfo.MESSAGETYPE_PICSENDFROMME:
                                                File file = new File("./tempfile");
                                                if(!file.exists()){
                                                    file.mkdir();
                                                }
                                                file = new File("./tempfile/"+count);
                                                if(!file.exists()){
                                                    file.mkdir();
                                                }
                                                FileOutputStream fos = new FileOutputStream(new File("./tempfile/"+count+"/"+messageInfo.getChatfile().getName()));
                                                byte[] receiveBytes = new byte[1024];
                                                int l = 0;
                                                long sum = 0;
                                                System.out.print( "Start receiving " + messageInfo.getChatfilelength() + ": ");
                                                while((l = dis.read(receiveBytes,0,receiveBytes.length)) > 0){
                                                    sum += l;
                                                    System.out.print( 100 * sum / messageInfo.getChatfilelength() +"%  ");
                                                    fos.write(receiveBytes,0,l);
                                                    fos.flush();
                                                    if(sum >= messageInfo.getChatfilelength()){
                                                        break;
                                                    }
                                                }
                                                fos.close();
                                                System.out.println("done");
                                                DBAccess.getDBA().execute("INSERT INTO filestore(chatid,filename,filelength,filepath) " +
                                                        "VALUES( "+ count + ",'" + messageInfo.getChatfile().getName() + "'," + messageInfo.getChatfilelength() +
                                                        ",'"+"./tempfile/"+count+"/"+messageInfo.getChatfile().getName()+"');");
                                                MessageInfo filemessageInfo = null;
                                                if(messageInfo.getMessagetype() == MessageInfo.MESSAGETYPE_FILESENDFROMME){
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(),"Send file: " + messageInfo.getChatfile().getName() + "  " + messageInfo.getChatfilelength() + "B",messageInfo.getMessagetype());
                                                }else {
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(),"***SEND A PIC***" + "    " + messageInfo.getChatfilelength() + "B",messageInfo.getMessagetype());
                                                }
                                                filemessageInfo.setChatfileid(count);
                                                filemessageInfo.setChatfile(messageInfo.getChatfile());
                                                filemessageInfo.setChatfilelength(messageInfo.getChatfilelength());
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_SENDMESSAGE, filemessageInfo));
                                                rs = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid="
                                                        + messageInfo.getReceiverid() + ";");
                                                rs.next();
                                                if(rs.getInt("online") == 1){
                                                    if(messageInfo.getMessagetype() == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                        filemessageInfo = new MessageInfo(findSocketID(ooslist, oos), "Send file: " + messageInfo.getChatfile().getName() + "  " + messageInfo.getChatfilelength() + "B", messageInfo.getMessagetype() + 1);
                                                    }else {
                                                        filemessageInfo = new MessageInfo(findSocketID(ooslist,oos),"***SEND A PIC***" + "    " + messageInfo.getChatfilelength() + "B",messageInfo.getMessagetype() + 1);
                                                    }
                                                    filemessageInfo.setChatfileid(count);
                                                    filemessageInfo.setChatfile(messageInfo.getChatfile());
                                                    filemessageInfo.setChatfilelength(messageInfo.getChatfilelength());
                                                    ooslist[messageInfo.getReceiverid()].writeObject(new ResponseObject(ResponseObject.RES_SENDMESSAGE, filemessageInfo));
                                                }
                                                break;
                                            case MessageInfo.MESSAGETYPE_FILERECEIVE:
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_FILERECEIVE,0));
                                                File fileDirectory = new File("./tempfile/"+messageInfo.getChatfileid());
                                                file = fileDirectory.listFiles()[0];
                                                FileInputStream fis = new FileInputStream(file);
                                                byte[] sendBytes = new byte[1024];
                                                l = 0;
                                                sum = 0;
                                                System.out.print( "Start sending " + file.getName() + ": ");
                                                while((l = fis.read(sendBytes,0,sendBytes.length)) > 0){
                                                    sum += l;
                                                    System.out.print( 100 * sum / file.length() +"%  ");
                                                    dos.write(sendBytes,0,l);
                                                    dos.flush();
                                                }
                                                fis.close();
                                                System.out.println("done");
                                                break;
                                        }
                                        break;
                                    case RequestObject.REQ_LOADHISTORYCHAT:
                                        LoadhistorychatInfo loadhistorychatInfo = (LoadhistorychatInfo)reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT sendtimestamp FROM chatstore WHERE (senderid=" +
                                                findSocketID(ooslist,oos) + " AND receiverid=" + loadhistorychatInfo.getID() + ") OR (senderid=" +
                                                loadhistorychatInfo.getID() + " AND receiverid=" + findSocketID(ooslist,oos) + ") ORDER BY BINARY sendtimestamp ASC");
                                        date = null;
                                        Date predate = null;
                                        format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                        count = 0;
                                        while(rs.next()){
                                            if (date == null){
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                count++;
                                            }else{
                                                predate = date;
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                if((date.getTime()-predate.getTime()) > perioddate * 1000){
                                                    count++;
                                                }
                                            }
                                            count ++;
                                        }
                                        count++;
                                        MessageInfo[] messageset = new MessageInfo[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM chatstore WHERE (senderid=" +
                                                findSocketID(ooslist,oos) + " AND receiverid=" + loadhistorychatInfo.getID() + ") OR (senderid=" +
                                                loadhistorychatInfo.getID() + " AND receiverid=" + findSocketID(ooslist,oos) + ") ORDER BY BINARY sendtimestamp ASC");
                                        date = null;
                                        predate = null;
                                        count = 0;
                                        while(rs.next()){
                                            if (date == null){
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs.getString("sendtimestamp"),MessageInfo.MESSAGETYPE_TIMEINFO);
                                            }else{
                                                predate = date;
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                if((date.getTime()-predate.getTime()) > perioddate * 1000){
                                                    messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs.getString("sendtimestamp"),MessageInfo.MESSAGETYPE_TIMEINFO);
                                                }
                                            }
                                            switch (rs.getInt("messagetype")){
                                                case MessageInfo.MESSAGETYPE_TEXTSENDFROMME:
                                                    ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT content FROM textstore WHERE chatid=" + rs.getInt("chatid")+";");
                                                    rs2.next();
                                                    if(rs.getInt("senderid") == loadhistorychatInfo.getID()){
                                                        messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs2.getString("content"),MessageInfo.MESSAGETYPE_TEXTSENDFROMOTHERS);
                                                    }else{
                                                        messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs2.getString("content"),MessageInfo.MESSAGETYPE_TEXTSENDFROMME);
                                                    }
                                                    break;
                                                case MessageInfo.MESSAGETYPE_FILESENDFROMME: case MessageInfo.MESSAGETYPE_PICSENDFROMME:
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT filename,filelength FROM filestore WHERE chatid=" + rs.getInt("chatid")+";");
                                                    rs2.next();
                                                    MessageInfo filemessageInfo = null;
                                                    if(rs.getInt("senderid") == loadhistorychatInfo.getID()){
                                                        if(rs.getInt("messagetype") == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "Send file: " + rs2.getString("filename") + "  " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_FILESENDFROMOTHERS);
                                                        }else {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "***SEND A PIC***" + "    " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_PICSENDFROMOTHERS);
                                                        }
                                                    }else{
                                                        if(rs.getInt("messagetype") == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "Send file: " + rs2.getString("filename") + "  " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_FILESENDFROMME);
                                                        }else {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "***SEND A PIC***" + "    " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_PICSENDFROMME);
                                                        }
                                                    }
                                                    filemessageInfo.setChatfileid(rs.getInt("chatid"));
                                                    File file = new File("./tempfile/"+rs.getInt("chatid"));
                                                    filemessageInfo.setChatfile(file.listFiles()[0]);
                                                    filemessageInfo.setChatfilelength(file.listFiles()[0].length());
                                                    messageset[count++] = filemessageInfo;
                                                    break;
                                            }
                                        }
                                        messageset[count] = new MessageInfo(loadhistorychatInfo.getID(),"-----Above is history chat record.-----",MessageInfo.MESSAGETYPE_TIMEINFO);
                                        oos.writeObject(new ResponseObject(ResponseObject.RES_LOADHISTORYCHAT,new LoadhistorychatInfoRes(loadhistorychatInfo.getID(),messageset)));
                                        break;
                                    case RequestObject.REQ_CREATENEWGROUP:
                                        NewGroupInfo newGroupInfo = (NewGroupInfo)reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM groups;");
                                        rs.next();
                                        int groupid = rs.getInt("COUNT(*)");
                                        DBAccess.getDBA().execute("INSERT INTO groups(groupid,groupname) " +
                                                "VALUES("+groupid + ",'" + newGroupInfo.getGroupName()+"');");
                                        DBAccess.getDBA().executeUpdate("CREATE TABLE group_" + groupid + "(" +
                                                "uid INT(11) NOT NULL PRIMARY KEY," +
                                                "uname VARCHAR(20) BINARY NOT NULL);");
                                        for(int memberid : newGroupInfo.getGroupmembers()){
                                            rs = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid="+ memberid+";");
                                            rs.next();
                                            DBAccess.getDBA().execute("INSERT INTO group_" + groupid + "(uid,uname) " +
                                                    "VALUES(" + memberid + ",'"+ rs.getString("uname") + "');");
                                            DBAccess.getDBA().execute("INSERT INTO joinedgroup_" + memberid + "(groupid,groupname) " +
                                                    "VALUES(" + groupid + ",'" + newGroupInfo.getGroupName()+ "');");
                                        }
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid,uname FROM users WHERE uid="+ findSocketID(ooslist,oos)+";");
                                        rs.next();
                                        DBAccess.getDBA().execute("INSERT INTO group_" + groupid + " " +
                                                "VALUES(" + rs.getInt("uid") + ",'"+ rs.getString("uname") + "');");
                                        DBAccess.getDBA().execute("INSERT INTO joinedgroup_" + rs.getInt("uid") + "(groupid,groupname) " +
                                                "VALUES(" + groupid + ",'" + newGroupInfo.getGroupName()+ "');");
                                        break;
                                    case RequestObject.REQ_LOADGROUPLIST:
                                        LoadgrouplistInfo loadgrouplistInfo = (LoadgrouplistInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM joinedgroup_"+ loadgrouplistInfo.getID() +
                                                " ;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        int[] grouplist = new int[count];
                                        String[] groupname = new String[count];
                                        int[] groupmembers = new int[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT groupid,groupname FROM joinedgroup_"+ loadgrouplistInfo.getID() + ";");
                                        i = 0;
                                        while(rs.next()){
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM group_" + rs.getInt("groupid") + ";");
                                            rs2.next();
                                            grouplist[i] = rs.getInt("groupid");
                                            groupname[i] = rs.getString("groupname");
                                            groupmembers[i++] = rs2.getInt("COUNT(*)");
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADGROUPLIST,new LoadgrouplistInfoRes(grouplist,groupname,groupmembers));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_SENDGROUPMESSAGE:
                                        messageInfo = (MessageInfo)reqObj.getReqBody();
                                        date = new Date();
                                        format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                        time = format.format(date);
                                        rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM groupchatstore;");
                                        rs.next();
                                        count = rs.getInt("COUNT(*)");
                                        if(messageInfo.getMessagetype()!= MessageInfo.MESSAGETYPE_FILERECEIVE) {
                                            DBAccess.getDBA().execute("INSERT INTO groupchatstore(chatid,senderid,groupid,messagetype,sendtimestamp) " +
                                                    "VALUES(" + count + "," + findSocketID(ooslist, oos) + "," + messageInfo.getReceiverid() + "," + messageInfo.getMessagetype() +
                                                    ",'" + time + "');");
                                        }
                                        switch (messageInfo.getMessagetype()){
                                            case MessageInfo.MESSAGETYPE_TEXTSENDFROMME:
                                                DBAccess.getDBA().execute("INSERT INTO grouptextstore(chatid,content) " +
                                                        "VALUES(" +count+",'"+messageInfo.getChatmessage()+"');");
                                                MessageInfo messageInfoRes = new MessageInfo(messageInfo.getReceiverid(),messageInfo.getChatmessage(),messageInfo.getMessagetype());
                                                rs = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid=" +
                                                        findSocketID(ooslist,oos) + ";");
                                                rs.next();
                                                messageInfoRes.setSendername(rs.getString("uname"));
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_SENDGROUPMESSAGE,messageInfoRes));
                                                messageInfoRes = new MessageInfo(messageInfo.getReceiverid(),messageInfo.getChatmessage(),messageInfo.getMessagetype()+1);
                                                messageInfoRes.setSendername(rs.getString("uname"));
                                                rs = DBAccess.getDBA().executeQuery("SELECT uid FROM group_"
                                                        + messageInfo.getReceiverid() + " WHERE uid!=" + findSocketID(ooslist,oos) + ";");
                                                while(rs.next()){
                                                    ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid=" +
                                                            rs.getInt("uid") + ";");
                                                    rs2.next();
                                                    if(rs2.getInt("online") == 1){
                                                        ooslist[rs.getInt("uid")].writeObject(new ResponseObject(ResponseObject.RES_SENDGROUPMESSAGE, messageInfoRes));
                                                    }
                                                }
                                                break;
                                            case MessageInfo.MESSAGETYPE_FILESENDFROMME: case MessageInfo.MESSAGETYPE_PICSENDFROMME:
                                                File file = new File("./grouptempfile");
                                                if(!file.exists()){
                                                    file.mkdir();
                                                }
                                                file = new File("./grouptempfile/"+count);
                                                if(!file.exists()){
                                                    file.mkdir();
                                                }
                                                FileOutputStream fos = new FileOutputStream(new File("./grouptempfile/"+count+"/"+messageInfo.getChatfile().getName()));
                                                byte[] receiveBytes = new byte[1024];
                                                int l = 0;
                                                long sum = 0;
                                                System.out.print( "Start receiving " + messageInfo.getChatfile().getName() + ": ");
                                                while((l = dis.read(receiveBytes,0,receiveBytes.length)) > 0){
                                                    sum += l;
                                                    System.out.print( 100 * sum / messageInfo.getChatfilelength() +"%  ");
                                                    fos.write(receiveBytes,0,l);
                                                    fos.flush();
                                                    if(sum >= messageInfo.getChatfilelength()){
                                                        break;
                                                    }
                                                }
                                                fos.close();
                                                System.out.println("done");
                                                DBAccess.getDBA().execute("INSERT INTO groupfilestore(chatid,filename,filelength,filepath) " +
                                                        "VALUES( "+ count + ",'" + messageInfo.getChatfile().getName() + "'," + messageInfo.getChatfilelength() +
                                                        ",'"+"./grouptempfile/"+count+"/"+messageInfo.getChatfile().getName()+"');");
                                                MessageInfo filemessageInfo = null;
                                                if(messageInfo.getMessagetype() == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(), "Send file: " + messageInfo.getChatfile().getName() + "  " + messageInfo.getChatfilelength() + "B", messageInfo.getMessagetype());
                                                }else {
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(), "***SEND A PIC***" + "    " + messageInfo.getChatfilelength() + "B", messageInfo.getMessagetype());
                                                }
                                                rs = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid=" +
                                                        findSocketID(ooslist,oos) + ";");
                                                rs.next();
                                                filemessageInfo.setSendername(rs.getString("uname"));
                                                filemessageInfo.setChatfileid(count);
                                                filemessageInfo.setChatfile(messageInfo.getChatfile());
                                                filemessageInfo.setChatfilelength(messageInfo.getChatfilelength());
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_SENDGROUPMESSAGE, filemessageInfo));
                                                if(messageInfo.getMessagetype() == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(), "Send file: " + messageInfo.getChatfile().getName() + "  " + messageInfo.getChatfilelength() + "B", messageInfo.getMessagetype() + 1);
                                                }else {
                                                    filemessageInfo = new MessageInfo(messageInfo.getReceiverid(), "***SEND A PIC***" + "    " + messageInfo.getChatfilelength() + "B", messageInfo.getMessagetype() + 1);
                                                }
                                                filemessageInfo.setSendername(rs.getString("uname"));
                                                filemessageInfo.setChatfileid(count);
                                                filemessageInfo.setChatfile(messageInfo.getChatfile());
                                                filemessageInfo.setChatfilelength(messageInfo.getChatfilelength());
                                                rs = DBAccess.getDBA().executeQuery("SELECT uid FROM group_"
                                                        + messageInfo.getReceiverid() + " WHERE uid!=" + findSocketID(ooslist,oos) + ";");
                                                while(rs.next()){
                                                    ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid=" +
                                                            rs.getInt("uid") + ";");
                                                    rs2.next();
                                                    if(rs2.getInt("online") == 1){
                                                        ooslist[rs.getInt("uid")].writeObject(new ResponseObject(ResponseObject.RES_SENDGROUPMESSAGE, filemessageInfo));
                                                    }
                                                }
                                                break;
                                            case MessageInfo.MESSAGETYPE_FILERECEIVE:
                                                oos.writeObject(new ResponseObject(ResponseObject.RES_FILERECEIVE,0));
                                                File fileDirectory = new File("./grouptempfile/"+messageInfo.getChatfileid());
                                                file = fileDirectory.listFiles()[0];
                                                FileInputStream fis = new FileInputStream(file);
                                                byte[] sendBytes = new byte[1024];
                                                l = 0;
                                                sum = 0;
                                                System.out.print( "Start sending " + file.getName() + ": ");
                                                while((l = fis.read(sendBytes,0,sendBytes.length)) > 0){
                                                    sum += l;
                                                    System.out.print( 100 * sum / file.length() +"%  ");
                                                    dos.write(sendBytes,0,l);
                                                    dos.flush();
                                                }
                                                fis.close();
                                                System.out.println("done");
                                                break;
                                        }
                                        break;
                                    case RequestObject.REQ_LOADGROUPHISTORYCHAT:
                                        loadhistorychatInfo = (LoadhistorychatInfo)reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT sendtimestamp FROM groupchatstore WHERE groupid=" +
                                                loadhistorychatInfo.getID() + " ORDER BY BINARY sendtimestamp ASC");
                                        date = null;
                                        predate = null;
                                        format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                        count = 0;
                                        while(rs.next()){
                                            if (date == null){
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                count++;
                                            }else{
                                                predate = date;
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                if((date.getTime()-predate.getTime()) > perioddate * 1000){
                                                    count++;
                                                }
                                            }
                                            count ++;
                                        }
                                        count++;
                                        messageset = new MessageInfo[count];
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM groupchatstore WHERE groupid=" +
                                                loadhistorychatInfo.getID() + " ORDER BY BINARY sendtimestamp ASC");
                                        date = null;
                                        predate = null;
                                        count = 0;
                                        while(rs.next()){
                                            if (date == null){
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs.getString("sendtimestamp"),MessageInfo.MESSAGETYPE_TIMEINFO);
                                            }else{
                                                predate = date;
                                                date = format.parse(rs.getString("sendtimestamp"));
                                                if((date.getTime()-predate.getTime()) > perioddate * 1000){
                                                    messageset[count++] = new MessageInfo(loadhistorychatInfo.getID(),rs.getString("sendtimestamp"),MessageInfo.MESSAGETYPE_TIMEINFO);
                                                }
                                            }
                                            switch (rs.getInt("messagetype")){
                                                case MessageInfo.MESSAGETYPE_TEXTSENDFROMME:
                                                    ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT content FROM grouptextstore WHERE chatid=" + rs.getInt("chatid")+";");
                                                    rs2.next();
                                                    if(rs.getInt("senderid") == findSocketID(ooslist,oos)){
                                                        messageset[count] = new MessageInfo(loadhistorychatInfo.getID(),rs2.getString("content"),MessageInfo.MESSAGETYPE_TEXTSENDFROMME);
                                                    }else{
                                                        messageset[count] = new MessageInfo(loadhistorychatInfo.getID(),rs2.getString("content"),MessageInfo.MESSAGETYPE_TEXTSENDFROMOTHERS);
                                                    }
                                                    ResultSet rs3 = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid=" +
                                                            rs.getInt("senderid") + ";");
                                                    rs3.next();
                                                    messageset[count++].setSendername(rs3.getString("uname"));
                                                    break;
                                                case MessageInfo.MESSAGETYPE_FILESENDFROMME:case  MessageInfo.MESSAGETYPE_PICSENDFROMME:
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT filename,filelength FROM groupfilestore WHERE chatid=" + rs.getInt("chatid")+";");
                                                    rs2.next();
                                                    MessageInfo filemessageInfo = null;
                                                    if(rs.getInt("senderid") == findSocketID(ooslist,oos)){
                                                        if(rs.getInt("messagetype") == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "Send file: " + rs2.getString("filename") + "  " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_FILESENDFROMME);
                                                        }else {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "***SEND A PIC***" + "    " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_PICSENDFROMME);
                                                        }
                                                    }else{
                                                        if(rs.getInt("messagetype") == MessageInfo.MESSAGETYPE_FILESENDFROMME) {
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "Send file: " + rs2.getString("filename") + "  " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_FILESENDFROMOTHERS);
                                                        }else{
                                                            filemessageInfo = new MessageInfo(loadhistorychatInfo.getID(), "***SEND A PIC***" + "    " + rs2.getLong("filelength") + "B", MessageInfo.MESSAGETYPE_PICSENDFROMOTHERS);
                                                        }
                                                    }
                                                    filemessageInfo.setChatfileid(rs.getInt("chatid"));
                                                    File file = new File("./grouptempfile/"+rs.getInt("chatid"));
                                                    filemessageInfo.setChatfile(file.listFiles()[0]);
                                                    filemessageInfo.setChatfilelength(file.listFiles()[0].length());
                                                    rs3 = DBAccess.getDBA().executeQuery("SELECT uname FROM users WHERE uid=" +
                                                            rs.getInt("senderid") + ";");
                                                    rs3.next();
                                                    filemessageInfo.setSendername(rs3.getString("uname"));
                                                    messageset[count++] = filemessageInfo;
                                                    break;
                                            }
                                        }
                                        messageset[count] = new MessageInfo(loadhistorychatInfo.getID(),"-----Above is history chat record.-----",MessageInfo.MESSAGETYPE_TIMEINFO);
                                        oos.writeObject(new ResponseObject(ResponseObject.RES_LOADGROUPHISTORYCHAT,new LoadhistorychatInfoRes(loadhistorychatInfo.getID(),messageset)));
                                        break;
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            try {
                                clientsocket.close();
                            }catch (IOException ee){
                                ee.printStackTrace();
                            }
                            if(userid >= 0){
                                DBAccess.getDBA().execute("UPDATE users SET online=0 WHERE uid="
                                        +userid+";");
                            }
                        }
                    }
                }).start();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private static int findSocketID(ObjectOutputStream[] ooslist,ObjectOutputStream oos){
        for(int i = 0;i < ooslist.length;i++)
            if (ooslist[i] == oos)
                return i;
        return -1;
    }
}
