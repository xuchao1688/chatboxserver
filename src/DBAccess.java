
import java.io.*;
import java.sql.*;
import java.util.Scanner;

/**
 * Created by Administrator1 on 2016/10/29.
 */
public class DBAccess {
    private static DBAccess dba;
    private Connection connection = null;
    private String mName;
    private String mPass;
    private DBAccess(){
        try{
            Scanner in = new Scanner(System.in);
            System.out.print("please enter your mysql root name: ");
            mName = in.nextLine();
            System.out.print("please enter your mysql root password: ");
            //mPass = "Passw0rd";
            mPass = new String(System.console().readPassword());
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        getConnection();
    }

    public static DBAccess getDBA(){
        if (dba == null){
            dba = new DBAccess();
        }
        return dba;
    }
    public Connection getConnection(){
        String mUrl = "jdbc:mysql://localhost:3306/chatbox";
        try{
            connection = DriverManager.getConnection(mUrl,mName,mPass);
            System.out.println("Success to connect to " + connection);
        }catch (SQLException e){
            e.printStackTrace();
            if(connection == null){
                System.out.println("not found this db.Start to init!");
                initDataBase();
            }
        }
        return connection;
    }
    private void initDataBase(){
        Connection connection = null;
        String mUrl = "jdbc:mysql://localhost:3306/mysql";
        String mNewUrl = "jdbc:mysql://localhost:3306/chatbox";
        try{
            connection = DriverManager.getConnection(mUrl,mName,mPass);
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE DATABASE chatbox;");
            System.out.println("Success to connect to create chatbox!" );
            connection = DriverManager.getConnection(mNewUrl,mName,mPass);
            statement = connection.createStatement();
            statement.executeUpdate("CREATE  TABLE users(" +
                    "uid INT(11) NOT NULL PRIMARY KEY," +
                    "uname VARCHAR(20) BINARY NOT NULL," +
                    "password VARCHAR(20) BINARY NOT NULL, " +
                    "online TINYINT(4) NOT NULL);");
            statement.executeUpdate("CREATE  TABLE chatstore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "senderid INT(11) NOT NULL," +
                    "receiverid INT(11) NOT NULL ," +
                    "messagetype TINYINT(4) NOT NULL," +
                    "sendtimestamp VARCHAR(50) BINARY NOT NULL);");
            statement.executeUpdate("CREATE TABLE textstore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "content VARCHAR(300) BINARY NOT NULL);");
            statement.executeUpdate("CREATE TABLE filestore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "filename VARCHAR(50) BINARY NOT NULL," +
                    "filelength BIGINT NOT NULL," +
                    "filepath VARCHAR(50) BINARY NOT NULL);");
            statement.executeUpdate("CREATE  TABLE groups(" +
                    "groupid INT(11) NOT NULL PRIMARY KEY," +
                    "groupname VARCHAR(20) BINARY NOT NULL);");
            statement.executeUpdate("CREATE TABLE groupchatstore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "senderid INT(11) NOT NULL," +
                    "groupid INT(11) NOT NULL," +
                    "messagetype TINYINT(4) NOT NULL," +
                    "sendtimestamp VARCHAR(50) BINARY NOT NULL);");
            statement.executeUpdate("CREATE TABLE grouptextstore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "content VARCHAR(300) BINARY NOT NULL);");
            statement.executeUpdate("CREATE TABLE groupfilestore(" +
                    "chatid INT(11) NOT NULL PRIMARY KEY," +
                    "filename VARCHAR(50) BINARY NOT NULL," +
                    "filelength BIGINT NOT NULL," +
                    "filepath VARCHAR(50) BINARY NOT NULL);");
            getConnection();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public void execute(String sqlExecute){
        try{
            PreparedStatement statement = connection.prepareStatement(sqlExecute);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String sqlExecute){
        ResultSet rs = null;
        try{
            PreparedStatement statement = connection.prepareStatement(sqlExecute);
            rs = statement.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }

    public void executeUpdate(String sqlExecute){
        try{
            PreparedStatement statement = connection.prepareStatement(sqlExecute);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

}
