import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/12/17.
 */

public class LoadhistorychatInfoRes implements Serializable {
    private int chatid;
    private MessageInfo[] chatmessage;
    public LoadhistorychatInfoRes(int id,MessageInfo[] message){
        chatmessage = message;
        chatid = id;
    }
    @Override
    public String toString(){
        return "chattoid is " + chatid + ", chatmessage number is " + chatmessage.length;
    }

    public MessageInfo[] getChatmessage(){
        return  chatmessage;
    }
    public int getChatid(){
        return  chatid;
    }
}
