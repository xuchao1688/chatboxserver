import java.io.File;
import java.io.Serializable;
import java.io.StreamCorruptedException;

/**
 * Created by Administrator1 on 2016/12/13.
 */
public class MessageInfo implements Serializable {
    private int receiverid;
    private String sendername;
    private String chatmessage;
    private int messagetype;
    private File chatfile;
    private int chatfileid;
    private long chatfilelength;
    public static final int MESSAGETYPE_TEXTSENDFROMME = 1;
    public static final int MESSAGETYPE_TEXTSENDFROMOTHERS = 2;
    public static final int MESSAGETYPE_TIMEINFO = 3;
    public static final int MESSAGETYPE_FILESENDFROMME = 4;
    public static final int MESSAGETYPE_FILESENDFROMOTHERS = 5;
    public static final int MESSAGETYPE_FILERECEIVE = 6;
    public static final int MESSAGETYPE_PICSENDFROMME = 7;
    public static final int MESSAGETYPE_PICSENDFROMOTHERS = 8;
    public MessageInfo(int id,String message,int type){
        receiverid = id;
        chatmessage = message;
        messagetype = type;
    }

    public void setChatfile(File file){
        chatfile = file;
    }

    public void setChatfileid(int id){
        chatfileid = id;
    }

    public void setChatfilelength(long length){
        chatfilelength = length;
    }

    public void setSendername(String name){
        sendername = name;
    }

    @Override
    public String toString(){
        return "receiverid is " + receiverid + ", messagetype is " + messagetype;
    }

    public int getReceiverid(){
        return receiverid;
    }

    public String getChatmessage(){
        return  chatmessage;
    }

    public int getMessagetype(){
        return  messagetype;
    }

    public  File getChatfile(){
        return chatfile;
    }

    public int getChatfileid(){
        return chatfileid;
    }

    public long getChatfilelength(){
        return chatfilelength;
    }

    public String getSendername(){
        return sendername;
    }
}
